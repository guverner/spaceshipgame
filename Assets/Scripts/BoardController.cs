﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace SpaceG
{
    public class BoardController : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private CustomCamera m_camera;
        [SerializeField]
        [Range(0, 100)]
        private float m_numPlanetsPercent = 30f;
        [SerializeField]
        private SectorView m_sectorPrefab;
        [SerializeField]
        private Transform m_sectorsContainer;

        private int m_sectorSize;//num cells
        private Dictionary<Vector2, Sector> m_sectors;
        #endregion

        #region Public
        public void Init(int sectorSize)
        {
            m_sectorSize = sectorSize;

            m_sectors = new Dictionary<Vector2, Sector>();
            CreateSector(Vector2.zero);
        }

        public void HideInvisibleSectors()
        {
            Vector2 camPos = m_camera.transform.localPosition;
            Vector2[] sectorsHave = m_sectors.Keys.ToArray();
            for(int i = sectorsHave.Length - 1; i >= 0; i--)
            {
                Vector2 sectorPos = sectorsHave[i];

                if(!IsSectorInsideCam(sectorPos, camPos))
                {
                    HideSector(sectorPos);
                }
            }
        }

        public void ShowVisibleSectors()
        {
            Vector2 camPos = m_camera.transform.localPosition;
            Vector2 currentSectorPos = GetSectorPos(m_camera.transform.localPosition);
            int side = Mathf.CeilToInt(m_camera.Size / (m_sectorSize / 2.0f)/* - 1*/);

            for(int y = (int)(-side + currentSectorPos.y); y <= side + currentSectorPos.y; y++)
            {
                for(int x = (int)(-side + currentSectorPos.x); x <= side + currentSectorPos.x; x++)
                {
                    Vector2 sectorPos = new Vector2(x, y);

                    if(IsSectorInsideCam(sectorPos, camPos))
                    {
                        ShowSector(sectorPos);
                    }
                }
            }
        }
        #endregion

        private void CreateSector(Vector2 pos)
        {
            Sector sector = new Sector();
            sector.Init(CreateSectorView(pos), m_sectorSize, m_numPlanetsPercent);

            m_sectors.Add(pos, sector);
        }

        private SectorView CreateSectorView(Vector2 pos)
        {
            SectorView sectorView = GameObject.Instantiate(m_sectorPrefab, m_sectorsContainer, false);
            sectorView.transform.localPosition =
                new Vector3(pos.x * m_sectorSize, pos.y * m_sectorSize, 0.0f);
            return sectorView;
        }

        private void ShowSector(Vector2 pos)
        {
            Sector sector = GetSector(pos);
            if(sector == null)
            {
                CreateSector(pos);
            }
            else if(sector.View != null)
            {
                sector.View.gameObject.SetActive(true);
            }
            else
            {
                sector.Recreate(CreateSectorView(pos));
            }
        }

        private void HideSector(Vector2 pos)
        {
            Sector sector = GetSector(pos);
            if(sector != null)
            {
                sector.DestroyView();
            }
        }

        private Sector GetSector(Vector2 pos)
        {
            Sector sector = null;
            m_sectors.TryGetValue(pos, out sector);
            return sector;
        }

        private bool IsSectorInsideCam(Vector2 segmentPos, Vector2 camPos)
        {
            Vector2 sectorWorldPos = segmentPos * m_sectorSize;
            Vector2 distFromCam = sectorWorldPos - camPos;
            float halfSegmentSize = m_sectorSize / 2.0f;

            return Mathf.Abs(distFromCam.x) - halfSegmentSize < m_camera.Size &&
                            Mathf.Abs(distFromCam.y) - halfSegmentSize < m_camera.Size;
        }

        private Vector2 GetSectorPos(Vector2 cellPos)
        {
            float halfSegmentSize = m_sectorSize / 2.0f;
            return new Vector2(
                Mathf.RoundToInt((cellPos.x / halfSegmentSize / 2.0f)),
                Mathf.RoundToInt(cellPos.y / halfSegmentSize / 2.0f));
        }
    }
}