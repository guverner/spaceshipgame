﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceG
{
    public class SectorView : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private PlanetView m_planetPrefab;
        #endregion

        public PlanetView InstantiatePlanet(Vector2 pos, int rating)
        {
            PlanetView planet = GameObject.Instantiate(m_planetPrefab, this.transform);
            planet.transform.localPosition = new Vector3(pos.x, pos.y, rating);
            planet.SetRatingText(rating);
            return planet;
        }
    }
}