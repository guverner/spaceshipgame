﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceG
{
    public class CustomCamera : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Camera m_camera;
        [SerializeField]
        private Transform m_cameraBorders;
        [SerializeField]
        private PinchZoom m_pinchZoom;
        [SerializeField]
        private BoardController m_boardController;
        [SerializeField]
        private RatingsRadar m_ratingsRadar;
        #endregion

        public float DefaultSize { get; private set; }
        public float Size {
            get { return m_camera.orthographicSize; }
            private set {
                m_camera.orthographicSize = value;
                ScaleBorders(value);
            }
        }

        #region Unity Events
        private void Awake()
        {
            DefaultSize = m_camera.orthographicSize;
        }

        private void Update()
        {
            float newSize = m_pinchZoom.CheckZoom();

            if(Size != newSize)
            {
                Size = newSize;

                m_boardController.ShowVisibleSectors();
                m_boardController.HideInvisibleSectors();
                StartCoroutine(m_ratingsRadar.ShowNearRatings());
            }
        }
        #endregion

        public void SetPos(Vector2 newPos)
        {
            this.transform.localPosition = new Vector3(newPos.x, newPos.y, this.transform.localPosition.z);
        }

        private void ScaleBorders(float newCamSize)
        {
            float newBordersScale = newCamSize / DefaultSize;
            m_cameraBorders.localScale = new Vector3(newBordersScale, newBordersScale, 1.0f);
        }
    }
}