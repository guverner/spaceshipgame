﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoveDir = SpaceG.MoveListener.MoveDir;

namespace SpaceG
{
    public class Player : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private CustomCamera m_camera;
        [SerializeField]
        private BoardController m_boardController;
        [SerializeField]
        private RatingsRadar m_ratingsRadar;
        #endregion

        public Vector2 Pos {
            get { return this.transform.localPosition; }
            private set { this.transform.localPosition = value; }
        }

        #region Unity Events
        private void OnTriggerEnter2D(Collider2D col)
        {
            m_camera.SetPos(Pos);
            m_boardController.ShowVisibleSectors();
            m_boardController.HideInvisibleSectors();
        }
        #endregion

        //called from Inspector
        public void Move(MoveDir moveDir)
        {
            if(moveDir == MoveDir.NONE) return;

            Vector2 newPos = Pos;

            switch(moveDir)
            {
                case MoveDir.UP:
                    newPos.y++;
                    break;
                case MoveDir.DOWN:
                    newPos.y--;
                    break;
                case MoveDir.LEFT:
                    newPos.x--;
                    break;
                case MoveDir.RIGHT:
                    newPos.x++;
                    break;
            }

            Pos = newPos;

            ChangeDirection(moveDir);

            StartCoroutine(m_ratingsRadar.ShowNearRatings());
        }

        public void ChangeDirection(MoveDir moveDir)
        {
            switch(moveDir)
            {
                case MoveDir.UP:
                    this.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    break;
                case MoveDir.DOWN:
                    this.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
                    break;
                case MoveDir.LEFT:
                    this.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);
                    break;
                case MoveDir.RIGHT:
                    this.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 270.0f);
                    break;
            }
        }
    }
}