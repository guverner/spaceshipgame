﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceG
{
    public class PinchZoom : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private CustomCamera m_camera;

        [SerializeField]
        private float m_touchZoomSpeed = 0.5f;
        [SerializeField]
        private float m_mouseZoomSpeed = 0.5f;
        [SerializeField]
        private float m_minOrthoSize;
        [SerializeField]
        private float m_maxOrthoSize;
        #endregion

        #region Unity Events
        public float CheckZoom()
        {
            float newOrthoSize = m_camera.Size;

            if(Application.isMobilePlatform)
            {
                if(Input.touchCount == 2)
                {
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    // Find the position in the previous frame of each touch.
                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    // Find the magnitude of the vector (the distance) between the touches in each frame.
                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    // Find the difference in the distances between each frame.
                    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                    newOrthoSize += deltaMagnitudeDiff * m_touchZoomSpeed;
                }
            }
            else
            {
                newOrthoSize += Input.mouseScrollDelta.y * m_mouseZoomSpeed;
            }

            if(newOrthoSize < m_minOrthoSize)
                newOrthoSize = m_minOrthoSize;
            else if(newOrthoSize > m_maxOrthoSize)
                newOrthoSize = m_maxOrthoSize;

            return newOrthoSize;

            //if(m_camera.Size != newOrthoSize)
            //{
            //    m_camera.Size = newOrthoSize;

            //    Vector2 camPos = m_camera.transform.localPosition;

            //    m_boardBuilder.ShowVisibleSegments(camPos);
            //    m_boardBuilder.HideInvisibleSegments(camPos);
            //}
        }
        #endregion
    }
}