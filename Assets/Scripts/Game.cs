﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceG
{
    public class Game : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Transform m_worldRoot;
        [SerializeField]
        private BoardController m_boardController;
        [SerializeField]
        private int m_numCellsAtStart = 5;
        [SerializeField]
        private RatingsRadar m_ratingsRadar;
        #endregion

        #region Unity Events
        private void Start()
        {
            SetupWorldScale();
            m_boardController.Init(m_numCellsAtStart);
            m_ratingsRadar.Init();
            StartCoroutine(m_ratingsRadar.ShowNearRatings());
        }
        #endregion

        #region Helpers
        private void SetupWorldScale()
        {
            Camera camera = Camera.main;

            float worldHeightOfScreen = camera.orthographicSize * 2.0f;
            float worldWidthOfScreen = camera.aspect * worldHeightOfScreen;

            float cellHeight = worldHeightOfScreen / m_numCellsAtStart;
            float cellWidth = worldWidthOfScreen / m_numCellsAtStart;

            m_worldRoot.localScale = new Vector3(cellWidth, cellHeight, 1.0f);
        }
        #endregion
    }
}