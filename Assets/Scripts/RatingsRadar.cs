﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceG
{
    public class RatingsRadar : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Player m_player;
        [SerializeField]
        private int m_numRatingsShow = 10;
        [SerializeField]
        private CustomCamera m_customCamera;

        private int m_planetMask;
        private PlanetView[] m_shownPlanets;
        #endregion

        public void Init()
        {
            m_planetMask = LayerMask.GetMask("Planet");
            m_shownPlanets = new PlanetView[m_numRatingsShow];
        }

        public IEnumerator ShowNearRatings()
        {
            yield return null;

            HidePrevPlanetRatings();

            int rectSize = 3;
            float camFullSize = m_customCamera.Size * 2.0f;

            List<Collider2D> planets = new List<Collider2D>(10);

            Collider2D[] collidersArray = Physics2D.OverlapBoxAll(
                m_player.transform.position, new Vector2(rectSize, rectSize), 0, m_planetMask);

            planets.AddRange(collidersArray);

            while(planets.Count < m_numRatingsShow && rectSize <= camFullSize)
            {
                rectSize += 2;

                collidersArray = Physics2D.OverlapBoxAll(
                    m_player.transform.position, new Vector2(rectSize, rectSize), 0, m_planetMask);

                for(int i = collidersArray.Length - 1; i >= 0; i--)
                {
                    Collider2D collider = collidersArray[i];

                    if(!planets.Contains(collider))
                        planets.Add(collider);
                }
            }

            int numPlanetsFound = 0;
            foreach(Collider2D collider in planets)
            {
                PlanetView planetView = collider.GetComponent<PlanetView>();
                planetView.SetShowRating(true);

                m_shownPlanets[numPlanetsFound] = planetView;
                numPlanetsFound++;

                if(numPlanetsFound >= m_numRatingsShow)
                {
                    break;
                }
            }
        }

        private void HidePrevPlanetRatings()
        {
            if(m_shownPlanets != null)
            {
                foreach(PlanetView planetView in m_shownPlanets)
                {
                    if(planetView != null)
                        planetView.SetShowRating(false);
                }
            }
        }
    }
}