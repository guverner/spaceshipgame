﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceG
{
    public class PlanetView : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Text ratingText;
        [SerializeField]
        private GameObject m_canvas;
        #endregion

        public void SetRatingText(int rating)
        {
            ratingText.text = rating.ToString();
        }

        public void SetShowRating(bool isShow)
        {
            m_canvas.SetActive(isShow);
        }
    }
}