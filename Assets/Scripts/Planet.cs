﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceG
{
    public class Planet
    {
        public int Rating { get; private set; }

        public void Init(int rating)
        {
            Rating = rating;
        }
    }
}