﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceG
{
    public class Sector
    {
        public SectorView View { get; private set; }
        public Dictionary<Vector2, Planet> Planets { get; private set; }

        private const int MIN_RATING = 1;
        private const int MAX_RATING = 10000;

        public void Init(SectorView sectorView, int numCellsSide, float numPlanetsPercent)
        {
            View = sectorView;

            if(Planets != null)
            {
                foreach(var item in Planets)
                {
                    PlanetView planetView = View.InstantiatePlanet(item.Key, item.Value.Rating);
                }
            }
            else
            {
                int numCellsTotal = numCellsSide * numCellsSide;
                int numPlanets = (int)(numCellsTotal * numPlanetsPercent / 100.0f);

                if(numPlanets > 0)
                {
                    Planets = new Dictionary<Vector2, Planet>(numPlanets);

                    int[] ratings = GetSortedRandRatings(numPlanets);

                    for(int i = 0; i < numPlanets;)
                    {
                        Vector2 planetPos = new Vector2(
                            Random.Range(-numCellsSide / 2, (numCellsSide / 2) + 1),
                            Random.Range(-numCellsSide / 2, (numCellsSide / 2) + 1));

                        if(!Planets.ContainsKey(planetPos))
                        {
                            Planet planet = new Planet();
                            planet.Init(ratings[i]);
                            Planets.Add(planetPos, planet);

                            PlanetView planetView = View.InstantiatePlanet(planetPos, planet.Rating);
                            i++;
                        }
                    }
                }
            }
        }

        private int[] GetSortedRandRatings(int num)
        {
            int[] ratings = new int[num];

            for(int i = 0; i < num; i++)
            {
                ratings[i] = Random.Range(MIN_RATING, MAX_RATING + 1);
            }

            System.Array.Sort(ratings, (int a, int b) => { return b.CompareTo(a); });

            return ratings;
        }

        public void Recreate(SectorView sectorView)
        {
            Init(sectorView, 0, 0);
        }

        public void DestroyView()
        {
            if(View != null)
            {
                GameObject.Destroy(View.gameObject);
                View = null;
            }
        }
    }
}