﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceG
{
    public class MoveListener : MonoBehaviour
    {
        [System.Serializable]
        private class MoveEvent : UnityEvent<MoveDir>
        {
        }

        public enum MoveDir { NONE, UP, DOWN, LEFT, RIGHT }

        #region Fields
        [SerializeField]
        private bool m_isMoveByKeyboard;
        [SerializeField]
        private MoveEvent m_moveEvent;
        private Vector2 m_touchOrigin = -Vector2.one;//screen coordinates
        #endregion

        #region Unity Events
        private void Update()
        {
            MoveDir moveDir = MoveDir.NONE;

            if(Application.isMobilePlatform)
            {
                if(Input.touchCount == 1)
                {
                    Touch myTouch = Input.GetTouch(0);

                    if(myTouch.phase == TouchPhase.Began)
                    {
                        m_touchOrigin = myTouch.position;
                    }
                    else if(myTouch.phase == TouchPhase.Ended && m_touchOrigin.x >= 0)
                    {
                        moveDir = DetermineMoveDir(m_touchOrigin, myTouch.position);
                        m_touchOrigin.x = -1;//reset
                    }
                }
                else
                {
                    m_touchOrigin.x = -1;//reset
                }
            }
            else
            {
                if(m_isMoveByKeyboard)
                {
                    if(Input.anyKeyDown)
                    {
                        int vertical = (int)(Input.GetAxisRaw("Vertical"));
                        int horizontal = (int)(Input.GetAxisRaw("Horizontal"));

                        if(vertical > 0)
                            moveDir = MoveDir.UP;
                        else if(vertical < 0)
                            moveDir = MoveDir.DOWN;
                        else if(horizontal < 0)
                            moveDir = MoveDir.LEFT;
                        else if(horizontal > 0)
                            moveDir = MoveDir.RIGHT;
                    }
                }
                else
                {
                    bool isMouseLeftDown = Input.GetMouseButton(0);

                    if(isMouseLeftDown && m_touchOrigin.x < 0)//click first time
                    {
                        m_touchOrigin = Input.mousePosition;
                    }
                    else if(!isMouseLeftDown && m_touchOrigin.x >= 0)//finish click
                    {
                        moveDir = DetermineMoveDir(m_touchOrigin, Input.mousePosition);
                        m_touchOrigin.x = -1;//reset
                    }
                }
            }

            if(moveDir != MoveDir.NONE && m_moveEvent != null)
            {
                m_moveEvent.Invoke(moveDir);
            }
        }
        #endregion

        private MoveDir DetermineMoveDir(Vector2 origin, Vector2 end)
        {
            if(origin == end)
            {
                return MoveDir.NONE;
            }

            float deltaX = end.x - origin.x;
            float deltaY = end.y - origin.y;

            if(Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
            {
                return deltaX > 0 ? MoveDir.RIGHT : MoveDir.LEFT;
            }
            else
            {
                return deltaY > 0 ? MoveDir.UP : MoveDir.DOWN;
            }
        }
    }
}